public class GCDF {
    public static int gcd(int a,int b) {
        while (b !=0) {
            int tmp = Math.floorMod(a,b);
            a = b;
            b = tmp;
        }
        return a;
    }
}
