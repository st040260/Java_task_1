public class GCDR {
    public static int gcd(int a,int b) {
        while (b !=0) {
            int tmp = Math.abs(a%b);
            a = b;
            b = tmp;
        }
        return a;
    }
}